#!/bin/bash

# if [ $# -lt 1 ]; then
#     echo "usage: compile.sh paper"
#     exit;
# fi

TARGET="flowmap"

HOME=$(pwd)
LOG="$HOME/compile.log"
PDFLATEX="pdflatex --shell-escape"
BIBTEX="bibtex"

echo "Pdflatex version: $($PDFLATEX -version | head -1)"
echo "Bibtex version: $($BIBTEX -version | head -1)"

cd $HOME/src

$PDFLATEX $TARGET > $LOG
bibtex $TARGET >> $LOG
$PDFLATEX $TARGET >> $LOG
$PDFLATEX $TARGET >> $LOG

cd $HOME

echo "Compiling finished!"
exit
