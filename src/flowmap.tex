\documentclass[10pt,conference]{IEEEtran}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% My preamble file to define useful package
% importing and commands definition.
%
% Version 0.1.1
% Xiaming Chen - chenxm35@gmail.com
%
%% Math
\usepackage[cmex10]{amsmath}
\usepackage{amssymb}
\usepackage{array}
\usepackage{mathtools}
\usepackage{amsthm}

%% Algorithms
\usepackage{algorithmicx}
\usepackage{algorithm}
\usepackage{algpseudocode}

%% Tables
\usepackage{threeparttable,booktabs}
\usepackage{etoolbox}
\appto\TPTnoteSettings{\footnotesize}
\usepackage{multirow}
\usepackage[table]{xcolor}

%% Graphics and figures
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{epsfig}
\usepackage{subfig}

%% Miscs
\usepackage{fixltx2e}
\usepackage{url}
% \usepackage{cite}
\usepackage{paralist}

%% Theorems
\theoremstyle{plain}% default
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary} % without numbering

%% Definition and example
\theoremstyle{definition}
\newtheorem{defnn}{Definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem*{exmpn}{Example}
\newtheorem{exmp}{Example}[section]

%% Other remarks and notes
\theoremstyle{remark}
\newtheorem*{rem}{Remark}
\newtheorem*{note}{Note}
\newtheorem{case}{Case}

%% Definition equality
\newcommand{\defeq}{\vcentcolon=}
\newcommand{\eqdef}{=\vcentcolon}

%% Argument min/max
\newcommand{\argmin}{\mathop{\arg\min}}
\newcommand{\argmax}{\mathop{\arg\max}}

%% Bold emphasis
\newcommand{\emphbf}[1]{\emph{\textbf{#1}}}
\newcommand{\bftable}{\fontseries{b}\selectfont}

%% Figure files
\graphicspath{ {../figures/} }
\DeclareGraphicsExtensions{.eps,.pdf}

%% Set Letter paper size
%\setlength{\paperheight}{11in}
%\setlength{\paperwidth}{8.5in}
%\usepackage[pass]{geometry}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{ The Flow Emergence Model and Universal Laws of Human Mobility Dynamics
  \thanks{ This work was supported by National Natural Science Foundation of
    China (Grant No. 61371084).} }

\author{
  \IEEEauthorblockN{
    Xiaming Chen\IEEEauthorrefmark{1},
    Ying Sun\IEEEauthorrefmark{1},
    Siwei Qiang\IEEEauthorrefmark{1},
    Kaiyuan Shi\IEEEauthorrefmark{1}, Yaohui
    Jin\IEEEauthorrefmark{1}\IEEEauthorrefmark{2} }

  \IEEEauthorblockA{
    \IEEEauthorrefmark{1}State Key Lab of Advanced Optical Communication
    System and Network\\
    \IEEEauthorrefmark{2}Network and Information Center\\
    Shanghai Jiao Tong University, P. R. China\\
    jinyh@sjtu.edu.cn }
}

\maketitle


\begin{abstract}
  Understanding universal properties of human movement contributes a
  significant value to mobile network management, economical analysis, and
  epidemic spreading control etc. For a long time, statistical laws of
  metapopulation and physical individual patterns of human mobility are two
  main aspects that have drawn a large amount of studies in behaviour
  science. Although, the relation and interactive principles between them have
  not been addressed sufficiently, primarily due to the lack of multiscale data
  and efficient methods to explore the complicated phenomena and universal
  properties in human behaviours.  With the help of digital mobility traces at
  varying spatial resolutions, we propose and theoretically validate the Flow
  Emergence Model, namely \emph{Flowmap}, to cover the lost land between
  physical observations and statistical laws of human mobility.
\end{abstract}

\begin{IEEEkeywords}
  Mobility Network, Model Robustness, Multi-resolution, Behaviour Science
\end{IEEEkeywords}

%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
\section{Introduction}
\label{sec:intro}
%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Mobility network is a vital substrate during the development history of our
human beings from the commodity exchange to population and culture
expansion. Since the seminal migration model proposed by Ravenstein
\cite{ravenstein_laws_1885} a hard century ago, generations of social and
physical scientists have made great efforts towards universal laws that
naturally govern human movement.  Understanding human mobility patterns is
consequential for multiple areas such as demographic investigation
\cite{ratti_redrawing_2010}, urban transport plan \cite{daqing_spatial_2014},
epidemic prediction \cite{balcan_multiscale_2009}, and the national estimation
of economic development \cite{eagle_network_2010}.

Nowadays, pervasive technologies with mobile devices and wearable sensors bring
about a novel era to inspect human movement at an unprecedented depth and width
scale, which makes it possible to uncover the universal laws ever masked by
limited observations. This kind of data is usually captured by complicated
systems such as communication and currency networks, and remains consistent
data quality about individual trajectories for a large population. However, the
inter-system quality typically reveals inconsistency as a result of topological
difference and underlying mechanism, e.g., mobile networks record individual
trajectories \cite{gonzalez_understanding_2008} while currency systems picture
the circulation of banknotes \cite{brockmann_money_2008}. For these reasons,
robust models explaining the universal mobility patterns in multiple scenarios
are still demanded, especially when considering the intrinsic quality
inconsistency among varying sources.

The prevailing modelling of human mobility is involving a particle system
within a spatial area. A particle representing an individual moves freely or
with predefined (statistical) laws in the domain. Brownian motion succeeds in
characterizing the random process and inspires a large family of Random Walk
models. Recent discoveries in behaviour science challenge these simplistic
assumptions about complex human activities nevertheless. Several pivotal
observations of human mobility have not yet been covered by random-family
models:

\textbf{P1.} Human activities illustrate non-Poisson statistics, characterized
by bursts of rapidly occurring events with heavy-tailed distribution of
inter-event periods \cite{barabasi_origin_2005}.

\textbf{P2.} The travelling distance is not uniformly distributed, a large
amount of short travels followed by occasional further displacements known as
L\'evy Flights \cite{brockmann_scaling_2006}.

\textbf{P3.} The number of distinct locations $L(t)$ for an individual takes an
empirically ultraslow growth as time proceeds, i.e., $L(t) \sim t^s, s < 1$,
right against the linear tendency ($=1$) for isotropic random walks
\cite{song_modelling_2010} and super-diffusive patterns ($>1$) for L\'evy
random walks with infinite step-length variance \cite{brockmann_scaling_2006}.

\textbf{P4.} People occupy locations with high revisiting preference, where the
$k_{th}$ most visited location follows a Zipf's law with $P(k) \sim k^{-1.5}$
\cite{gonzalez_understanding_2008}.

\textbf{P5.} An individual's mobility illustrates strong diurnal regularity and
patterns which can be approximated by a significant group of mobility motifs
\cite{schneider_unravelling_2013} or a weighted sum of primary eigenbehaviours
\cite{eagle_eigenbehaviors:_2009}.

Microscopic properties P1$\sim$P5 confirming the non-random nature of
individual's mobility provide a sound foundation for an accurate estimation of
mobility effects in a studied problem. Also, they are responsible for the
macroscopic observations, such as Gravity Law \cite{cochrane_possible_1975},
derived from aggregated movement data over space or time. In this line of work,
the number of individual's displacements (or \emph{metapopulation} in the
literature) is calculated for an origin-destination pair, and the distribution
of trips during specific interval of time is modelled to interpret human travel
patterns.

The statistical modelling of metapopulation \cite{stouffer_intervening_1940,
  cochrane_possible_1975, simini_universal_2012} and the physical modelling of
individual's movement \cite{pirozmand_human_2014, karamshuk_human_2011} have
parallel dominate the research of human mobility in behaviour science for a
long time. However, the relation and interactive principles between these two
groups have not been addressed sufficiently, primarily due to the lack of
multiscale data and efficient methods to explore the complicated phenomena and
universal laws in human behaviour. Conceptually, given a proposed physical
mobility model, macroscopic properties of user mobility can be observed via an
agent-based simulation within a large number of population. Actually, this is
not always true due to the inconsideration of information about underlying
mobility networks and the multiscale flow structure of individual's
movements. In this paper, we explore the connections between microscopic
mobility properties and macroscopic laws by proposing a novel mobility model,
i.e. Flow Emergence Model (FEM), and argue that the statistical phenomena of
mobility can be derived from a set of basic and simple rules embedded in
individual's movement.

The geographic structure of mobility has been addressed in many approaches;
they primarily focus on the spatial dimension rather than the spatiotemporal
dependence, or `flowing' nature of individual's movement. For example,


heterogeneity in large population, emergence, network structure

As also suggested by recently proposed Radiation model
\cite{simini_universal_2012},

The mobility network extracted from spatiotemporal flows conveys more
information about individual's habitats preference than the spatially cohesive
clusters.

challenges:

contributions



%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
\section{A Brief History of Mobility Models} \label{sec:relatedwork}
%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Ravenstein's migration laws \cite{ravenstein_laws_1885} has inspired a
long-term enthusiasm in understanding the relationship between human mobility
and distance. However, human mobility is inherently complex, similar to other
human-driven systems with undetermined underlying mechanisms and large-scale
heterogeneity in population. After eliminating the trivial and characteristic
parts of people's movement, common patterns and universal laws can be observed
at \emph{individual} and \emph{metapopulation} levels.

\subsection{Individual Mobility Models}

Surveys \cite{pirozmand_human_2014, karamshuk_human_2011}

\cite{gonzalez_understanding_2008} and \cite{brockmann_scaling_2006}: truncated
power-law

\subsection{Metapopulation Models}

\cite{ravenstein_laws_1885}


In the exhilarating journey from raw data to mathematical models, statistical
physics plays a key role, by quantifying complicated human activities as
stochastic processes and deriving the stable abstract through multiple concrete
situations. Simple models rooted in statistical physics, such as Colizza and
colleagues \cite{colizza_reactiondiffusion_2007}, are based on the ergodic
hypothesis \cite{}, where all accessible microstates are assumed equiprobable
over a long period of a time, then macroscopic laws are derived from observed
data averaged in space or time. From data perspective, population totals
between macro regions, rather than detailed movement trajectories of people,
are sufficient in consideration of for example, how a influenza driven by
physical proximity might spread through population.  The representative cases
of this category include Intervening Opportunity Model
\cite{stouffer_intervening_1940}, Gravity Model \cite{cochrane_possible_1975}
and the recent integration of Radiation Model \cite{simini_universal_2012} in
modeling metapopulation distribution and spatial disease dynamics.


%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
\section{Methodology} \label{sec:methodology}
%%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\subsection{A General Definition of Space-Time Trip}

Although human trips in space and time obtained different definition
forms in previous literature, a uniform definition and notation is
required to interpret individual and group mobility distributions in a
general framework. In brief, a space-time trip $\mathcal{T}$ consists
of a sequence of stations $O$\footnote{A stationary trip is a
  degenerated form of trip with mere one station, which is out of our
  considerations for the lack of mobility information.}  and a set of
flights $F$ that connect consecutive stations. A trip segment
$S_{i,j}$ fuses two particular stations, $O_i$ and $O_j$, and their
flight $F_{i,j}$ together to represent user displacement. Formally,
each trip segment is confined by a tuple of quantities
$S_{i,j} = (u, o, d, p_o, p_d, f)$ with user identity $u$, origin and
destination spots $o, d$, as well as their dwell times $p_o$ and
$p_d$, and the flight delay $f$.

\textbf{Resampling of trips}. A trip takes the form
$\mathcal{T} = \{ P_1, P_{1,2}, \ldots, P_{n-1, n} \}$

\section{Conclusion}

\bibliographystyle{abbrv}
\bibliography{flowmap}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% zotero-collection: #("131" 0 3 (name "Citation/Flowmap"))
%%% End:
